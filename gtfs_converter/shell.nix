with (import <nixpkgs> {});
let
  env = bundlerEnv {
    name = "gtfs_converter-bundler-env";
    inherit ruby;
    gemfile  = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset   = ./gemset.nix;
  };
in stdenv.mkDerivation {
  name = "gtfs_converter";
  buildInputs = [ env ruby ];
}
