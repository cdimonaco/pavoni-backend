# frozen_string_literal: true
require 'byebug'
require 'gtfs'
require 'json'

class GTFS::Stop
  def to_h
    vars = self.instance_variables
    vars.map do |x|
      n = x.to_s.gsub('@', '')
      { n => self.instance_variable_get(x) }
    end.reduce(&:merge)
  end
end

module Extract

  # Extract stops
  sources = {
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/FER/FER_GTFS.zip' => { city: 'Ferrara', company: 'FER' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/SETA/seta-modena-it-fixed.zip'  => { city: 'Modena', company: 'SETA' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/START/start-rimini-it-fixed.zip'=> { city: 'Rimini', company: 'START' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/START/start-ravenna-it-fixed.zip'=> { city: 'Ravenna', company: 'START' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/START/start-cesena-it-fixed.zip'=> { city: 'Cesena', company: 'START' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/TEP/tep-gtfs.zip' => { city: 'Parma', company: 'TEP' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/TPER/tper-bologna-it-fixed.zip'=> { city: 'Bologna', company: 'TPER' },
  '/home/zarel/code/pavoni/smau2019/data/GTFS/statici/TPER/tper-ferrara-it-fixed.zip'=> { city: 'Ferrara', company: 'TPER' }
  }

  ### STOPS
  all_stops = []
  gtfss = sources.keys.map do |src|
    parsed = GTFS::Source.build(src)
    begin
      stops = parsed.stops
      routes = parsed.routes
      all_stops << stops.map do |s|
        st = s.to_h
        st['lon']=st['lon'].to_f
        st['lat']=st['lat'].to_f
        st.merge(sources[src]).merge(
          {location: {lat: st['lat'], lon: st['lon']}}
        )
      end
    rescue Exception => exc
      # byebug
    end
  end.compact

  all_stops.flatten!

  # Save stops
  http = Net::HTTP.new('hackmover.soon.it', 443)
  http.use_ssl=true
  found = false
  all_stops.each do |stop|
    puts "stop #{stop['id']}"
	  found ||= stop['id'] == '600754'
	  next unless found
    uri = "/elastic/stops/_doc/#{stop['id']}"
    req = Net::HTTP::Put.new(uri)
    req.body = stop.to_json
    req.content_type = 'application/json'
    response = http.request(req)
  end

end
