defmodule Fake do
  def populate do
    {:ok, docs} = HackGw.Elastic.stops_nearby(44.9033, 10.5536, "15km")

    docs
    |> Enum.shuffle()
    |> Enum.chunk_every(2)
    |> Enum.map(fn [stop_in, stop_out] ->
      lat = stop_in |> Map.get("location") |> Map.get("lat")
      lon = stop_in |> Map.get("location") |> Map.get("lon")

      start = 1559889732 + rand()
      stop = start + ((100..10_000) |> Enum.random())

      %EventInfo{
        id: UUID.uuid4(:hex),
        location: {lat, lon},
        duration: (3..35) |> Enum.random,
        stops: {stop_name(stop_in), stop_name(stop_out)},
        ts: {start, stop},
        device: %{
          os: ["Android", "iOS"] |> Enum.random(),
          battery: 1..100 |> Enum.random()
        },
        carrier_id: ["TPER", "TEP", "START", "SETA", "FER"] |> Enum.random()
      }
    end)
    |> Enum.map(fn event -> HackGw.Elastic.send(event) end)
  end

  defp stop_name(m) do
    "#{Map.get(m, "city")}_#{Map.get(m, "company")}_#{Map.get(m, "name")}"
  end

  defp rand do
    (-10_000..10_000) |> Enum.random()
  end
end
