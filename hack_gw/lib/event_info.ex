defmodule EventInfo do
  defstruct(
    id: "",
    # {lat, lon}
    location: {0, 0},
    device: %{},
    # {stop_in, stop_out}
    stops: {:unk, :unk},
    # {ts_in, ts_out}
    ts: {0, 0},
    carrier_id: :unk,
    duration: 0
  )

  @type stop :: String.t() | atom()

  @type t :: %EventInfo{
          id: String.t(),
          # {lat, lon}
          location: {number(), number()},
          device: map(),
          # {stop_in, stop_out}
          stops: {stop(), stop()},
          # {ts_in, ts_out}
          ts: {number(), number()},
          carrier_id: String.t() | atom(),
          duration: number()
        }

  defimpl Jason.Encoder, for: EventInfo do
    def encode(value, opts) do
      {ts_in, ts_out} = value.ts
      {lat, lon} = value.location
      {stop_in, stop_out} = value.stops

      Jason.Encode.map(
        %{
          id: value.id,
          ts_in: ts_in,
          ts_out: ts_out,
          device: value.device,
          stop_in: stop_in,
          stop_out: stop_out,
          carrier_id: value.carrier_id,
          location: %{
            lat: lat,
            lon: lon
          }
        },
        opts
      )
    end
  end
end
