defmodule HackGw.Exporter do
  use GenServer

  @me __MODULE__
  @sender HackGw.Elastic

  def start_link(_) do
    GenServer.start_link(@me, [], name: @me)
  end

  @impl true
  def init(_) do
    {:ok, []}
  end

  @spec publish(EventInfo.t()) :: :ok
  def publish(ticket) do
    GenServer.call(@me, {:pub, ticket})
  end

  ##### CALLBACK #####

  @impl true
  def handle_call({:pub, ticket}, _from, state) do
    # send ticket to Elastic
    delivery_status = @sender.send(ticket)
    {:reply, delivery_status, state}
  end
end
