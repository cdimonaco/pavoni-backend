defmodule HackGw.Elastic do
  @behaviour HackGw.Exporter.Sender

  @elastic_url "https://hackmover.soon.it/elastic"

  @impl true
  def send(%EventInfo{} = ticket) do
    with {:ok, jticket} <- Jason.encode(ticket),
         {:ok, %HTTPoison.Response{status_code: 201}} <-
           HTTPoison.put("#{@elastic_url}/events/_doc/#{ticket.id}", jticket, [
             {"Content-Type", "application/json"}
           ]) do
      :ok
    else
      _ -> :error
    end
  end

  def stops_nearby(lat, lon, radius \\ "350m") do
    query = %{
      from: 0,
      size: 1_000,
      query: %{
        bool: %{
          must: %{match_all: %{}},
          filter: %{
            geo_distance: %{
              distance: radius,
              location: %{
                lat: lat,
                lon: lon
              }
            }
          }
        }
      }
    }

    query
    |> do_stops_nearby()
    |> parse_stops_nearby()
  end

  defp do_stops_nearby(query) do
    {:ok, jquery} = Jason.encode(query)

    request = %HTTPoison.Request{
      method: :get,
      url: "#{@elastic_url}/stops/_search",
      body: jquery,
      headers: [{"Accept", "application/json"}, {"Content-Type", "application/json"}]
    }

    case HTTPoison.request(request) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} -> {:ok, body}
      e -> e
    end
  end

  defp parse_stops_nearby({:ok, body}) do
    with {:ok, body} <- Jason.decode(body),
         hits <- Map.get(body, "hits"),
         docs <- Map.get(hits, "hits") do
      {:ok, docs |> Enum.map(fn d -> Map.get(d, "_source") end)}
    else
      _ -> :error
    end
  end

  defp parse_stops_nearby(e), do: e
end
