defmodule HackGw.Exporter.Sender do
  @callback send(EventInfo.t()) :: :ok | :error
end
