defmodule HackGwWeb.Router do
  use HackGwWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", HackGwWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", HackGwWeb do
    pipe_through :api

    post "/checkin", ApiController, :check_in
    post "/checkout", ApiController, :check_out
    post "/location_update", ApiController, :location_update

    get "/stops_nearby", ApiController, :stops_nearby
  end
end
