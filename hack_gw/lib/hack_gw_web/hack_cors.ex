defmodule HackGw.CORS do
  use Corsica.Router,
    origins: ["https://localhost:*", "http://localhost:*", ~r{^https?://(.*\.?)soon\.it$}],
    allow_credentials: true,
    max_age: 600

  resource "/public/*", origins: "*"
  resource "/*"
end
