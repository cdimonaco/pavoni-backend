defmodule HackGwWeb.PageController do
  use HackGwWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
