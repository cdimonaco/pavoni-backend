defmodule HackGwWeb.ApiController do
  use HackGwWeb, :controller
  alias HackGw.Worker

  def check_in(conn, %{"ts" => ts, "lat" => lat, "lon" => lon}) do
    id = new_id()
    r = Worker.check_in(id, %EventInfo{id: id, ts: {ts, 0}, location: {lat, lon}})
    json(conn, %{ok: r == :ok, id: id})
  end

  def check_in(conn, _), do: json(conn, %{ok: false, desc: "bad request"})

  def location_update(conn, %{"id" => id, "lat" => lat, "lon" => lon}) do
    r =
      id
      |> Worker.location_update(lat, lon)

    json(conn, %{ok: r})
  end

  def location_update(conn, _), do: json(conn, %{ok: false, desc: "bad request"})

  def check_out(conn, %{"id" => id, "ts" => ts_out, "stop" => stop_out}) do
    r =
      id
      |> Worker.check_out(ts_out, stop_out)

    json(conn, %{ok: r == :ok})
  end

  def check_out(conn, _), do: json(conn, %{ok: false, desc: "bad request"})

  def stops_nearby(conn, %{"lat" => lat, "lon" => lon}) do
    case HackGw.Elastic.stops_nearby(lat, lon) do
      {:ok, docs} -> json(conn, %{ok: true, docs: docs})
      _ -> json(conn, %{ok: false, desc: "invalid"})
    end
  end

  def stops_nearby(conn, _), do: json(conn, %{ok: false, desc: "bad request"})

  ##### PRIVATE #####

  @spec new_id() :: {number(), String.t()}
  defp new_id do
    UUID.uuid4(:hex)
  end
end
