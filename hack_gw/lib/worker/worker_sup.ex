defmodule HackGw.WorkerSup do
  use Supervisor

  def start_link(_) do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    Supervisor.init(workers(), strategy: :one_for_one)
  end

  def num_workers, do: 10

  defp workers do
    1..num_workers()
    |> Enum.map(fn id -> %{id: id, start: {HackGw.Worker, :start_link, [id]}} end)
  end
end
