defmodule HackGw.Worker do
  use GenServer

  @me __MODULE__
  alias HackGw.Exporter

  def start_link(id) do
    GenServer.start_link(@me, [], name: {:via, Registry, {Registry.Worker, id}})
  end

  @impl true
  def init(_) do
    {:ok, %{}}
  end

  ##### API #####

  @spec check_in(String.t(), EventInfo.t()) :: any()
  def check_in(id, %EventInfo{} = event_info) do
    id
    |> lookup()
    |> GenServer.call({:check_in, event_info})
  end

  @spec location_update(String.t(), number, number) :: any()
  def location_update(id, lat, lon) do
    id
    |> lookup()
    |> GenServer.call({:location_update, id, {lat, lon}})
  end

  @spec check_out(String.t(), number, number) :: any()
  def check_out(id, ts_out, stop_out) do
    id
    |> lookup()
    |> GenServer.call({:check_out, id, ts_out, stop_out})
  end

  ##### CALLBACK ######

  @impl true
  def handle_call({:check_in, event_info}, _from, state) do
    {:reply, :ok, Map.put(state, event_info.id, event_info)}
  end

  @impl true
  def handle_call({:check_out, id, ts_out, stop_out}, _from, state) do
    IO.inspect({:check_out, id, ts_out, stop_out})

    case Map.get(state, id) do
      nil ->
        {:reply, :error, state}

      current ->
        {stop_in, _} = current.stops
        {ts_in, _} = current.ts

        Exporter.publish(%{
          current
          | stops: {stop_in, stop_out},
            ts: {ts_in, ts_out}
        })

        {:reply, :ok, state}
    end
  end

  @impl true
  def handle_call({:location_update, id, {lat, lon}}, _from, state) do
    case Map.get(state, id) do
      nil ->
        {:reply, false, state}

      current ->
        {:reply, true, Map.put(state, id, %{current | location: {lat, lon}})}
    end
  end

  ##### PRIVATE ######

  @spec route_id(String.t()) :: number()
  defp route_id(id) do
    {route_id, _} = Integer.parse(id, 16)
    rem(route_id, HackGw.WorkerSup.num_workers())
  end

  @spec lookup(String.t()) :: pid()
  defp lookup(id) do
    [{pid, _}] = Registry.lookup(Registry.Worker, route_id(id))
    pid
  end
end
