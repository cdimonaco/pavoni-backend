# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :hack_gw, HackGwWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "F+nx6aTWtlwjUKy/TPOv4c0qAz5P80F6n48AV8TcHXXbMZ4YGqKgVF74K4X7BCjI",
  render_errors: [view: HackGwWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: HackGw.PubSub, adapter: Phoenix.PubSub.PG2]

config :hack_gw, workers: 10

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
